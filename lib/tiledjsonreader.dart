library tiledjsonreader;

import 'dart:convert';
import 'dart:io' as io;
import 'package:flutter/services.dart';

import 'map/tile_set_detail.dart';
import 'map/tiled_map.dart';
import 'tile_set/tile_set.dart';

class TiledJsonReader {
  final String pathFile;
  String? _basePathFile;
  String? _fileName;
  TiledMap? _map;

  TiledJsonReader(this.pathFile) {
    _fileName = pathFile.split('/').last;
    if (_fileName?.contains('.json') != true) {
      throw Exception('only supports json files');
    }
    _basePathFile = pathFile.replaceAll(_fileName ?? '', '');
  }
  
  // Load from local 
  Future<TiledMap> readFromLocal() async { 
    String data = await io.File(pathFile).readAsString(); 
    Map<String, dynamic> _result = jsonDecode(data); 
    _map = TiledMap.fromJson(_result); 
    if (_map?.tileSets != null) { 
      await Future.forEach(_map!.tileSets!, (TileSetDetail tileSet) async { 
        if (!tileSet.source!.contains('.json')) { 
          throw Exception('Invalid TileSet source: only supports json files'); 
        } 
        String data = await io.File( 
          '$_basePathFile${tileSet.source}', 
        ).readAsString(); 
        Map<String, dynamic> _result = jsonDecode(data); 
        tileSet.tileSet = TileSet.fromJson(_result); 
      }); 
    } 
    return Future.value(_map); 
  }

  // Load from rootBundle 
  Future<TiledMap> read() async { 
    String data = await rootBundle.loadString(pathFile); 
    Map<String, dynamic> _result = jsonDecode(data); 
    _map = TiledMap.fromJson(_result); 
    if (_map?.tileSets != null) { 
      await Future.forEach(_map!.tileSets!, (TileSetDetail tileSet) async { 
        if (!tileSet.source!.contains('.json')) { 
          throw Exception('Invalid TileSet source: only supports json files'); 
        } 
        String data = await rootBundle.loadString( 
          '$_basePathFile${tileSet.source}', 
        ); 
        Map<String, dynamic> _result = jsonDecode(data); 
        tileSet.tileSet = TileSet.fromJson(_result); 
      }); 
    } 
 
    return Future.value(_map); 
  }
}
